
public class Estudiante implements Persona {
	private String nombre;
	private String carrera;
	@Override
	public void setNombre(String nombre) {
		this.nombre = nombre;	
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return this.nombre;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

}
