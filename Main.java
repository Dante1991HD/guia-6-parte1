import java.util.HashSet;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		HashSet < String > estudiantes = new
				HashSet < >();
		Estudiante e1 = new Estudiante();
		e1.setNombre("Pedro Ramirez");
		Estudiante e2 = new Estudiante();
		e2.setNombre("Juan Perez");
		Estudiante e3 = new Estudiante();
		e3.setNombre("Diego Gonzalez");
		Estudiante e4 = new Estudiante();
		e4.setNombre("Calamardo Tentaculos");
		Estudiante e5 = new Estudiante();
		e5.setNombre("Bob Esponja");

		estudiantes.add(e1.getNombre());
		estudiantes.add(e2.getNombre());
		estudiantes.add(e3.getNombre());
		estudiantes.add(e4.getNombre());
		estudiantes.add(e5.getNombre());
		
		String estudiante ="Juan Perez";  
		System.out.println("El estudiante " + estudiante + (estudiantes.contains(estudiante) ? " pertenece al curso" : " no pertence al curso"));
		estudiante ="Dante Aguirre";  
		System.out.println("El estudiante " + estudiante + (estudiantes.contains(estudiante) ? " pertenece al curso" : " no pertence al curso"));
		Iterator < String > iterador = estudiantes.iterator();
		while (iterador.hasNext()) {
			System.out.println(iterador.next());
			 }

		HashSet < String > estudiantes2020 = new
				HashSet < >();
		Estudiante e6 = new Estudiante();
		e6.setNombre("Pedro Ramirez");
		Estudiante e7 = new Estudiante();
		e7.setNombre("Patricio Estrella");
		Estudiante e8 = new Estudiante();
		e8.setNombre("Diego Gonzalez");
		Estudiante e9 = new Estudiante();
		e9.setNombre("Dante Aguirre");
		Estudiante e10 = new Estudiante();
		e10.setNombre("Bob Esponja");

		estudiantes2020.add(e6.getNombre());
		estudiantes2020.add(e7.getNombre());
		estudiantes2020.add(e8.getNombre());
		estudiantes2020.add(e9.getNombre());
		estudiantes2020.add(e10.getNombre()); 
		estudiante ="Juan Perez";  
		System.out.println("El estudiante " + estudiante + (estudiantes2020.contains(estudiante) ? " pertenece al curso" : " no pertence al curso"));
		estudiante ="Dante Aguirre";  
		System.out.println("El estudiante " + estudiante + (estudiantes2020.contains(estudiante) ? " pertenece al curso" : " no pertence al curso"));
		Iterator < String > iterador2020 = estudiantes2020.iterator();
		while (iterador2020.hasNext()) {
			System.out.println(iterador2020.next());
			 }
		System.out.println(estudiantes2020.retainAll(estudiantes));

		//estudiantes2020.retainAll(estudiantes);
		System.out.println(estudiantes2020);

	}

}
